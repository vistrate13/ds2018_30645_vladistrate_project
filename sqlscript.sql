-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema restaurantdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema restaurantdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `restaurantdb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `restaurantdb` ;

-- -----------------------------------------------------
-- Table `restaurantdb`.`administrator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurantdb`.`administrator` (
  `idadministrator` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`idadministrator`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `restaurantdb`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurantdb`.`category` (
  `idcategory` INT NOT NULL AUTO_INCREMENT,
  `categoryname` VARCHAR(45) NULL,
  PRIMARY KEY (`idcategory`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `restaurantdb`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurantdb`.`products` (
  `idproducts` INT NOT NULL AUTO_INCREMENT,
  `productname` VARCHAR(45) NULL,
  `quantity` INT NULL,
  `category_idcategory` INT NOT NULL,
  PRIMARY KEY (`idproducts`, `category_idcategory`),
  INDEX `fk_products_category_idx` (`category_idcategory` ASC) VISIBLE,
  CONSTRAINT `fk_products_category`
    FOREIGN KEY (`category_idcategory`)
    REFERENCES `restaurantdb`.`category` (`idcategory`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `restaurantdb`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurantdb`.`users` (
  `idusers` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idusers`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
