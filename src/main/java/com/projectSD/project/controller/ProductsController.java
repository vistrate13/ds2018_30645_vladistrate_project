package com.projectSD.project.controller;

import com.projectSD.project.Model.Products;
import com.projectSD.project.repository.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class ProductsController {

    @Autowired
    ProductsRepository productsRepository;

    @GetMapping("/allProducts")
    @ResponseBody
    public Collection<Products> getAllProducts(){
        return productsRepository.findAll();
    }



}
