package com.projectSD.project.controller;

import com.projectSD.project.Model.Category;
import com.projectSD.project.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping("/allCategory")
    public Collection<Category>  getAllCategory(){
        return categoryRepository.findAll();
    }

    @GetMapping("/category")
    @ResponseBody
    public Category getCategoryById(@RequestParam(value = "id") Integer categoryId){
        return categoryRepository.getOne(categoryId);
    }

    @PostMapping("/category")
    public void createCategory(@Valid @RequestBody Category category){
        categoryRepository.save(category);
    }

    @DeleteMapping("/category/{categoryId}")
    public void deleteCategoryById(@PathVariable Integer categoryId){
         categoryRepository.deleteById(categoryId);
    }

    @PutMapping("category/{categoryId}")
    public void updateCategory(@PathVariable Integer categoryId, @Valid @RequestBody Category category){
         categoryRepository.findById(categoryId).map(category1 -> {
           category1.setNameCategory(category.getNameCategory());
           return categoryRepository.save(category1);
        });
    }
}
