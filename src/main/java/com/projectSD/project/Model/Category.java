package com.projectSD.project.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "category", catalog = "restaurantdb")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name ="idcategory")
    private Integer id;

    @Column(name = "categoryname")
    private String nameCategory;

    public Category(){

    }
    public void setId(Integer id) {
        this.id = id;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public Integer getId() {
        return id;
    }

    public String getNameCategory() {
        return nameCategory;
    }
}
